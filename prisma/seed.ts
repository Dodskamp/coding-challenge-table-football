import prisma from "../src/lib/db";

async function main() {
  const player1 = await prisma.player.create({
    data: {
      name: "Player 1",
    },
  });
  const player2 = await prisma.player.create({
    data: {
      name: "Player 2",
    },
  });
  const player3 = await prisma.player.create({
    data: {
      name: "Player 3",
    },
  });
  const player4 = await prisma.player.create({
    data: {
      name: "Player 4",
    },
  });

  const game1 = await prisma.game.create({
    data: {
      players: {
        create: [
          {
            player: {
              connect: {
                id: player1.id,
              },
            },
          },
          {
            player: {
              connect: {
                id: player2.id,
              },
            },
          },
        ],
      },
    },
  });

  const game2 = await prisma.game.create({
    data: {
      players: {
        create: [
          {
            player: {
              connect: {
                id: player3.id,
              },
            },
          },
          {
            player: {
              connect: {
                id: player4.id,
              },
            },
          },
        ],
      },
    },
  });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

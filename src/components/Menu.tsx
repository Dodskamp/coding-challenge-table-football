"use client";
import React, { useContext, useState } from "react";
import { TrophyOutlined, HomeOutlined, TeamOutlined } from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Menu } from "antd";

const items: MenuProps["items"] = [
  {
    label: (
      <a href="/" rel="noopener noreferrer">
        Dashboard
      </a>
    ),
    key: "dashboard",
    icon: <HomeOutlined />,
  },
  {
    label: (
      <a href="/games" rel="noopener noreferrer">
        Games
      </a>
    ),
    key: "games",
    icon: <TrophyOutlined />,
  },
];

const App: React.FC = () => {
  return (
    <Menu
      mode="horizontal"
      items={items}
      style={{ lineHeight: "64px", marginBottom: "20px" }}
    />
  );
};

export default App;

"use client";
import { Game } from "@prisma/client";
import { Button, Card, Flex, Input, message } from "antd";
import { useEffect, useRef, useState } from "react";
import { UserOutlined } from "@ant-design/icons";

export default function Game({ params }: { params: { id: string } }) {
  const [game, setGame] = useState({} as any);
  const refPlayer1 = useRef(null);
  const refPlayer2 = useRef(null);
  const [messageApi, contextHolder] = message.useMessage();

  useEffect(() => {
    const getGame = async () => {
      const response = await fetch(`/api/games/${params.id}`);
      const data = await response.json();

      setGame(data);
    };
    getGame();
  }, [params.id, game]);

  const saveScore = () => {
    const player1ScoreRef: any = refPlayer1.current;
    const player2ScoreRef: any = refPlayer2.current;

    // update game with goals
    const player1Score = player1ScoreRef.input.value;
    const player2Score = player2ScoreRef.input.value;
    fetch(`/api/games/${game.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ game, player1Score, player2Score }),
    })
      .catch(() => {
        messageApi.error(
          "An error occured while updating the game. Please try again."
        );
      })
      .finally(() => {
        messageApi.success("Game successfully updated !");
      });
  };

  const finishGame = () => {
    fetch(`/api/games/${game.id}`, {
      method: "DELETE",
    })
      .catch(() => {
        messageApi.error(
          "An error occured while finishing the game. Please try again."
        );
      })
      .finally(() => {
        messageApi.success("Game finished successfully !");
      });
  };

  if (game && game.players && game.players.length === 2) {
    return (
      <div>
        {contextHolder}
        <h2>Game {game.id}</h2>
        <Flex gap="middle">
          <Button
            type="primary"
            onClick={saveScore}
            style={{ marginBottom: 16 }}
            disabled={game.done}
          >
            Save score
          </Button>
          <Button type="primary" onClick={finishGame} disabled={game.done}>
            Finish the game
          </Button>
        </Flex>
        <Flex gap="middle">
          <Card style={{ width: 300 }}>
            <h2>{game.players[0].player.name}</h2>
            <Input
              size="large"
              prefix={<UserOutlined />}
              id="player1_goals"
              ref={refPlayer1}
              defaultValue={game.players[0].goals}
              disabled={game.done}
            />
          </Card>

          <Card style={{ width: 300 }}>
            <h2>{game.players[1].player.name}</h2>
            <Input
              size="large"
              prefix={<UserOutlined />}
              id="player2_goals"
              ref={refPlayer2}
              defaultValue={game.players[1].goals}
              disabled={game.done}
            />
          </Card>
        </Flex>
      </div>
    );
  }
}

"use client";
import { Button, Input, Modal, Skeleton, Table, message } from "antd";
import { UserOutlined, CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { useCallback, useEffect, useRef, useState } from "react";
import { Game } from "@prisma/client";

export default function Games() {
  const [isModalNewGameOpen, setIsModalNewGameOpen] = useState(false);
  const refPlayer1 = useRef(null);
  const refPlayer2 = useRef(null);
  const [isLoading, setIsLoading] = useState(true);
  const [games, setGames] = useState([] as Game[]);
  const [messageApi, contextHolder] = message.useMessage();

  const showModalNewGame = () => {
    setIsModalNewGameOpen(true);
  };

  const handleNewGameOk = () => {
    setIsModalNewGameOpen(false);
    const player1: any = refPlayer1.current;
    const player2: any = refPlayer2.current;
    if (player1 && player2) {
      const gameData = [
        { name: player1.input.value },
        { name: player2.input.value },
      ];

      fetch("/api/games", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(gameData),
      })
        .finally(() => {
          messageApi.success("Game successfully created !");
          updateData();
        })
        .catch((error) => {
          messageApi.error(
            "An error occured while updating the game. Please try again."
          );
          console.error(error);
        });
    }
  };

  const updateData = useCallback(async () => {
    const response = await fetch("/api/games");
    const data = await response.json();

    setGames(data);
    setIsLoading(false);
  }, []);

  useEffect(() => {
    updateData();
  }, [updateData]);

  const handleNewGameCancel = () => {
    setIsModalNewGameOpen(false);
  };

  const columns = [
    {
      title: "Game",
      dataIndex: "game",
      key: "game",
    },
    {
      title: "Player 1",
      dataIndex: "player_1",
      key: "player_1",
    },
    {
      title: "Player 1 goals",
      dataIndex: "player_1_goals",
      key: "player_1_goals",
    },
    {
      title: "Player 2",
      dataIndex: "player_2",
      key: "player_2",
    },
    {
      title: "Player 2 goals",
      dataIndex: "player_2_goals",
      key: "player_2_goals",
    },
    {
      title: "Link",
      dataIndex: "link",
      key: "link",
    },
    {
      title: "Is finished ?",
      dataIndex: "finished",
      key: "finished",
    },
  ];

  if (games) {
    return (
      <div>
        {contextHolder}
        <Button
          type="primary"
          onClick={showModalNewGame}
          style={{ marginBottom: "20px" }}
        >
          Start a game
        </Button>
        <Modal
          title="Create a new game"
          open={isModalNewGameOpen}
          onOk={handleNewGameOk}
          onCancel={handleNewGameCancel}
        >
          <div>
            <Input
              placeholder="Player 1 name"
              prefix={<UserOutlined />}
              style={{ marginBottom: "10px" }}
              ref={refPlayer1}
              id="player1"
            />
            <Input
              placeholder="Player 2 name"
              prefix={<UserOutlined />}
              ref={refPlayer2}
              id="player2"
            />
          </div>
        </Modal>
        {isLoading ? (
          <Skeleton />
        ) : (
          <Table
            dataSource={games.map((game: any) => {
              return {
                key: game.id,
                game: game.id,
                player_1: game.players[0].player.name,
                player_1_goals: game.players[0].goals,
                player_2: game.players[1].player.name,
                player_2_goals: game.players[1].goals,
                link: <a href={`/games/${game.id}`}>Link</a>,
                finished: game.done ? <CheckOutlined /> : <CloseOutlined />,
              };
            })}
            columns={columns}
          />
        )}
      </div>
    );
  }
}

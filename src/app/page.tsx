"use client";
import { Alert, Skeleton, Table } from "antd";
import { useCallback, useEffect, useState } from "react";
import { Player } from "@prisma/client";

export default function Home() {
  const [players, setPlayers] = useState([] as Player[]);
  const [isLoading, setIsLoading] = useState(true);

  const updateData = useCallback(async () => {
    const response = await fetch("/api/players");
    const data = await response.json();
    setPlayers(data);
    setIsLoading(false);
  }, []);

  useEffect(() => {
    updateData();
  }, [updateData]);

  const columns = [
    {
      title: "Player name",
      dataIndex: "player_name",
      key: "player_name",
    },
    {
      title: "Game played",
      dataIndex: "game_played",
      key: "game_played",
    },
    {
      title: "Wins",
      dataIndex: "wins",
      key: "wins",
    },
    {
      title: "Losses",
      dataIndex: "losses",
      key: "losses",
    },
    {
      title: "Ratio (Games Played/Wins)",
      dataIndex: "ratio",
      key: "ratio",
    },
    {
      title: "Goals For",
      dataIndex: "goals_for",
      key: "goals_for",
    },
    {
      title: "Goals Against",
      dataIndex: "goals_against",
      key: "goals_against",
    },
    {
      title: "Goals Difference",
      dataIndex: "goals_difference",
      key: "goals_difference",
    },
  ];

  if (players) {
    return (
      <div>
        <Alert
          message="Only finished games are displayed"
          type="info"
          showIcon
          style={{ marginBottom: "20px" }}
        />
        {isLoading ? (
          <Skeleton />
        ) : (
          <Table
            dataSource={players
              .map((player: any) => {
                return {
                  key: player.id,
                  player_name: player.name,
                  game_played: player.gamesPlayed,
                  wins: player.wins,
                  losses: player.looses,
                  ratio: (player.wins / player.gamesPlayed).toFixed(2),
                  goals_for: player.goalsFor,
                  goals_against: player.goalsAgainst,
                  goals_difference: player.goalsFor - player.goalsAgainst,
                };
              })
              .sort((a: any, b: any) => b.ratio - a.ratio)}
            columns={columns}
          />
        )}
      </div>
    );
  }
}

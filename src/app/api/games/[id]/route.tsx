import prisma from "@/lib/db";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  const param = request.url.split("/").pop() as string;
  const id = parseInt(param);
  const game = await prisma.game
    .findUnique({
      where: {
        id,
      },
      include: {
        players: {
          include: {
            player: true,
          },
        },
      },
    })
    .finally(() => {
      prisma.$disconnect();
    });

  return NextResponse.json(game);
}

export async function PUT(request: Request) {
  const body = await request.json();
  const game = body.game;
  const player1NewScore = parseInt(body.player1Score as string);
  const player2NewScore = parseInt(body.player2Score as string);

  const updatedGame = await prisma.game
    .update({
      where: {
        id: game.id,
      },
      data: {
        players: {
          updateMany: [
            {
              where: {
                gameId: game.id,
                playerId: game.players[0].playerId,
              },
              data: {
                goals: player1NewScore,
              },
            },
            {
              where: {
                gameId: game.id,
                playerId: game.players[1].playerId,
              },
              data: {
                goals: player2NewScore,
              },
            },
          ],
        },
      },
    })
    .finally(() => {
      prisma.$disconnect();
    });

  return NextResponse.json(updatedGame);
}

export async function DELETE(request: Request) {
  const param = request.url.split("/").pop() as string;
  const id = parseInt(param);
  const game = await prisma.game.findUnique({
    where: {
      id,
    },
    include: {
      players: {
        include: {
          player: true,
        },
      },
    },
  });

  let winnerId = null;

  if (game) {
    if (game.players[0].goals > game.players[1].goals) {
      winnerId = game.players[0].playerId;
    } else {
      winnerId = game.players[1].playerId;
    }
  }

  const gameUpdate = await prisma.game
    .update({
      where: {
        id,
      },
      data: {
        done: true,
        winnerId: winnerId,
      },
    })
    .finally(() => {
      prisma.$disconnect();
    });
  return NextResponse.json(gameUpdate);
}

import prisma from "@/lib/db";
import { NextResponse } from "next/server";

export async function GET() {
  const games = await prisma.game
    .findMany({
      include: {
        players: {
          include: {
            player: true,
          },
        },
      },
    })
    .finally(() => {
      prisma.$disconnect();
    });
  return NextResponse.json(games);
}

export async function POST(request: Request) {
  const body = await request.json();

  const player1 = body[0];
  const player2 = body[1];

  let player1_db = await prisma.player.findFirst({
    where: {
      name: {
        equals: player1.name,
      },
    },
  });
  if (!player1_db) {
    player1_db = await prisma.player.create({
      data: {
        name: player1.name,
      },
    });
  }
  let player2_db = await prisma.player.findFirst({
    where: {
      name: {
        equals: player2.name,
      },
    },
  });
  if (!player2_db) {
    player2_db = await prisma.player.create({
      data: {
        name: player2.name,
      },
    });
  }

  const game = await prisma.game
    .create({
      data: {
        players: {
          create: [
            {
              player: {
                connect: {
                  id: player1_db.id,
                },
              },
            },
            {
              player: {
                connect: {
                  id: player2_db.id,
                },
              },
            },
          ],
        },
      },
      include: {
        players: {
          include: {
            player: true,
          },
        },
      },
    })
    .finally(() => {
      prisma.$disconnect();
    });

  return NextResponse.json(game);
}

import prisma from "@/lib/db";
import { NextResponse } from "next/server";

export async function GET() {
  const players = await prisma.player
    .findMany({
      include: {
        games: {
          include: {
            game: {
              include: {
                players: true,
              },
            },
          },
        },
      },
    })
    .finally(() => {
      prisma.$disconnect();
    });

  /**
   * Calculates the total number of goals scored against a player.
   *
   * @param {number} id - The ID of the player.
   * @return {number} The total number of goals scored against the player.
   */
  function getGoalsAgainst(id: number) {
    let goalsAgainst = 0;

    players.map((p) => {
      p.games.forEach((g) => {
        if (g.game.players.some((p) => p.playerId === id)) {
          if (g.playerId !== id && g.game.done) {
            goalsAgainst += g.goals;
          }
        }
      });
    });

    return goalsAgainst;
  }

  function getWins(id: number) {
    let wins = 0;
    players.filter((p) => {
      p.games.forEach((g) => {
        if (g.playerId === id) {
          if (g.game.winnerId === id && g.game.done) {
            wins++;
          }
        }
      });
    });
    return wins;
  }

  function getLooses(id: number) {
    let looses = 0;
    players.filter((p) => {
      p.games.forEach((g) => {
        if (g.playerId === id) {
          if (g.game.winnerId !== id && g.game.done) {
            looses++;
          }
        }
      });
    });
    return looses;
  }

  const computedPlayers = players.map((player) => {
    return {
      ...player,
      gamesPlayed: player.games.filter((g) => g.game.done).length,
      goalsFor: player.games
        .filter((g) => g.game.done)
        .reduce((acc, game) => acc + game.goals, 0),
      goalsAgainst: getGoalsAgainst(player.id),
      wins: getWins(player.id),
      looses: getLooses(player.id),
    };
  });
  return NextResponse.json(computedPlayers);
}

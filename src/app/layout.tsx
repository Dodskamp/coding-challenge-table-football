import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import StyledComponentsRegistry from "../lib/AntdRegistry";
import Menu from "../components/Menu";
import { ConfigProvider } from "antd";
const inter = Inter({ subsets: ["latin"] });

import theme from "@/app/theme/mainTheme";

export const metadata: Metadata = {
  title: "Table Football Coding Challenge",
  description: "Table Football Coding Challenge",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <ConfigProvider theme={theme}>
          <Menu />
          <StyledComponentsRegistry>{children}</StyledComponentsRegistry>
        </ConfigProvider>
      </body>
    </html>
  );
}

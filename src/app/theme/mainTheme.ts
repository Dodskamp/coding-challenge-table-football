import type { ThemeConfig } from "antd";

const theme: ThemeConfig = {
  token: {
    fontSize: 16,
    colorPrimary: "#118ab2",
    colorLink: "#06D6A0",
    colorLinkHover: "#073B4C",
    colorText: "#073B4C",
  },
};

export default theme;

## CONTEXT

This is a [Next.js](https://nextjs.org/) project for the `TABLE FOOTBALL CODING CHALLENGE`

## DATABASE

A PostgreSQL database is used for this project. I used PgAdmin 4 for the admin interface. The ORM used in this project to do the link between the app and the database is [Prisma](https://www.prisma.io/).

- You can download PostgreSQL here : https://www.postgresql.org/download/windows/
- And PGAdmin here : https://www.pgadmin.org/download/pgadmin-4-windows/

### Schema

![db schema](./doc/db_schema.png)

## DESIGN

[Antd](https://ant.design/) is used to manage the design.

## How to run this project

### NODE.JS AND NPM

Install `nodejs` and `npm` to execute some commands later : https://nodejs.org/en/download

### NPX

If `npx` is not installed in your local computer, install it with command :

```bash
npm install -g npx
```

### PostgreSQL & PGAdmin

If not already done, install PostgreSQL and PGAdmin (optional) on your local machine.

### Project dependencies

Install all the required dependencies

```bash
npm install
```

### Prepare your database

1. Create a database called `coding-challenge-table-football`
   - ![db_creation_1](./doc/db_creation_1.png)
   - ![db_creation_2](./doc/db_creation_2.png)
2. > npx prisma db push
3. > npx prisma db seed (Optional, it will create 4 players with 2 games)

### ENV FILE

1. Copy the `.env.example` file and name it `.env`.
2. In `.env` file, replace `user:password` by your database username and password
3. If necessary, replace `localhost:5432` by the address and port of your database

### RUN !

Finally, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## HOW TO USE TABLE FOOTBALL WEBSITE

### `Games` page

You can see all started and finished games. You can also create a new game by hitting the `Start a game` and type the name of players. If the player doesn't exist, he'll be created.

In the table, you can see if the game is finished or not and by clicking on `Link` see and update the data (add score for players) for the game.

![games](./doc/games.png)

### `Games/[id]` page

You can here set player's goals by entering them in inputs and hit the `Save score` button.
You can also finish the game by hitting the `Finish the game`. After that, the stats for this game will be shown on the dashboad page.

![game](./doc/game.png)

If the game is finished all inputs are disabled.

![game finished](./doc/game_finished.png)

### `Dashboard` page

You can see here stats for all the players (only for finished games). The table is sorted by `Ratio (Games Played/Wins)`

![dashboard](./doc/dashboard.png)

## SWAGGER

You can use the api definition written in `api-definition.yaml` file to have a swagger for the api. You can use this page : https://editor-next.swagger.io/ (just copy/paste into it)
